package com.example.agendaarraylist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    private TableLayout tblLista;
    private ArrayList<Contacto> contactos;
    private ArrayList<Contacto> storedContactos;
    private int currentIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        tblLista = (TableLayout) findViewById(R.id.tblLista);

        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");
        storedContactos = contactos;
        currentIndex = (int) bundleObject.getInt("CurrentIndex");
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                Bundle oBundle = new Bundle();
                oBundle.putSerializable("storedContactos", storedContactos);
                oBundle.putInt("CurrentIndex", currentIndex);
                oBundle.putBoolean("nuevo", true);
                i.putExtras(oBundle);
                setResult(RESULT_OK,i);
                finish();

            }
        });
        cargarContactos();

    }
    public void cargarContactos(){
        for(int x = 0; x < contactos.size(); x++){
            final Contacto c = new Contacto(contactos.get(x));
            TableRow nRow = new TableRow(ListActivity.this);

            TextView nText = new TextView(ListActivity.this);
            nText.setText(c.getNombre());

            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nText.setTextColor((c.isFavorito())? Color.BLUE:Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(ListActivity.this);
            nButton.setText(R.string.accver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            nButton.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto", (Contacto) view.getTag(R.string.contacto_g));
                    oBundle.putInt("index",(int) c.getID());
                    //oBundle.putInt("index",(int) view.getTag(R.string.index));

                    oBundle.putSerializable("storedContactos", storedContactos);

                    oBundle.putInt("CurrentIndex", currentIndex);
                    oBundle.putBoolean("nuevo", false);
                    i.putExtras(oBundle);
                    setResult(RESULT_OK,i);
                    finish();
                }
            });
            nButton.setTag(R.string.contacto_g,c);
            //ponderar la linea siguiente
            //nButton.setTag(R.string.index,x);
            nButton.setTag(R.string.index,c.getID());
            nRow.addView(nButton);


            Button btnBorrar = new Button(ListActivity.this);
            btnBorrar.setText("Borrar");
            btnBorrar.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            btnBorrar.setTextColor(Color.BLACK);
            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for(int y = 0; y < storedContactos.size(); y++){
                        if(storedContactos.get(y).getID() == (int) c.getID()){
                            storedContactos.remove(storedContactos.get(y));
                            contactos = storedContactos;
                        }
                    }
                    clearLista();
                }
            });
            btnBorrar.setTag(R.string.contacto_g,c);
            //ponderar la linea siguiente
            //btnBorrar.setTag(R.string.index,x);
            btnBorrar.setTag(R.string.index,c.getID());
            nRow.addView(btnBorrar);

            tblLista.addView(nRow);

        }
    }

    private void buscar(String busqueda)
    {
        ArrayList<Contacto> list = new ArrayList<>();
        for(int x = 0; x < storedContactos.size(); x++){
            if(storedContactos.get(x).getNombre().toLowerCase().contains(busqueda.toLowerCase()))
            {
                list.add(storedContactos.get(x));
            }
        }
        contactos = list;

        clearLista();

    }
    private void clearLista(){
        tblLista.removeAllViews();
        TableRow headRow = new TableRow(ListActivity.this);
        TextView nombre = new TextView(ListActivity.this);
        nombre.setText(R.string.nombre);
        nombre.setTextSize(TypedValue.COMPLEX_UNIT_PT,8);
        TextView accion = new TextView(ListActivity.this);
        accion.setText(R.string.accion);
        accion.setTextSize(TypedValue.COMPLEX_UNIT_PT,8);
        headRow.addView(nombre);
        headRow.addView(accion);
        tblLista.addView(headRow);
        cargarContactos();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.txtBuscar);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                buscar(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

}
